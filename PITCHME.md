---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[north text-20]

## XeScala

@snapend

@snap[midpoint text-15]

#### A Scala Plugin for<br> XES Files

@snapend

@snap[south text-8]
###### Eugenio **Liso** --- Alessandro **Marino**
@snapend

---
@title[Repository Source Code]
You can find the source code at <br>[XeScala GitLab Repository](https://gitlab.com/amarino/xescala)

@snap[south]
@fa[code fa-2x]
@snapend

---?include=template/md/xes-docs/PITCHME.md

---?include=template/md/project-structure/PITCHME.md

---?include=template/md/flow/PITCHME.md
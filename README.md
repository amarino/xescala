# XeScala

This plugin works with `Netlogo v. 6.0.4`. It allows to parse and write XES files.

A presentation of this work, made using GitPitch, can be found at this link:
```bash
https://gitpitch.com/amarino/xescala/master?grs=gitlab
```
In general, you can see the slides at the following link (substitute the variables marked as $):

```bash
https://gitpitch.com/amarino/xescala/$branch?grs=gitlab
```

## Build

Download and install `sbt` version 1.2.8 from https://www.scala-sbt.org/download.html. Also, it is recommended that Java 8 (no particular update needed) is installed in your system (because otherwise `sbt` might not work well). Run `sbt compile package` from the root project. In the `target/scala-2.12` folder, you should see a `xes.jar` file. 
Copy it with the jars in the `lib` folder to the `app/extensions/xes` folder of Netlogo (create that directory if it not exists).

At this point, you can use in Netlogo the primitives of this package by adding the extension with `extensions [ xes ]`.

## Examples

There is an example model in the folder `samples`, along with some examples of xes files.

## Logging

If you want to have some logs of the extension plugin, follow this steps: 

1. Copy the content of `logs/netlogo_logging.xml` to the `app` folder of Netlogo
2. run Netlogo with `./NetlogoLogging` instead of `./Netlogo`.
3. Insert a random string (even blank) on the popup

When you call a primitive of the extension, you will find some logs in the Temp folder 
(it depends on the system. Check https://ccl.northwestern.edu/netlogo/docs/logging.html#where-logs-are-stored).
In the temp folder, you will find a file named like `logfile_fileAppender_2019-04-01.15_16_09_376.xml`.

Each time you start Netlogo, a new log file will be created.

## Caveat

When you write a XES file, make sure it has a `.xes` extension. Otherwise the parser will 
fail to read it.

## Future goals

- Customize how a XES file is written. Right now it is hardcoded (you get a list of traces with one event each)
- Check if all the XES data types can be written to a XES file (string, boolean etc...)
- Organize the code in a better way and optimize it
- Investigate how logging works and why some logs are written to stdout and not on log file
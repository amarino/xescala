name := "xes"

version := "0.1"

scalaVersion := "2.12.8"

enablePlugins(org.nlogo.build.NetLogoExtension)

netLogoVersion      := "6.0.4"

netLogoClassManager := "org.nlogo.extensions.xes.XeScalaClassManager"

netLogoExtName      := "org.nlogo.extensions.xes.XeSExtension"

netLogoZipSources   := true

isSnapshot := true

// https://mvnrepository.com/artifact/log4j/log4j
libraryDependencies += "log4j" % "log4j" % "1.2.17"

// Changes the name to <name>.jar
artifactName := { (_: ScalaVersion, _: ModuleID, artifact: Artifact) =>
  name.value + "." + artifact.extension
}
package org.nlogo.extensions.xes

import java.io.{FileNotFoundException, IOException}

import org.apache.log4j.Logger
import org.deckfour.xes.model.XLog
import org.nlogo.api._
import org.nlogo.core.Syntax._
import org.nlogo.core.{LogoList, Syntax}
import org.nlogo.extensions.xes.core.XesReader._
import org.nlogo.extensions.xes.core.XesWriter
import org.nlogo.nvm.ExtensionContext

case class ToFile(dump: AnyRef => String) extends Command {

  private val logger: Logger = Logger.getLogger(classOf[ToFile])

  override def getSyntax: Syntax = commandSyntax(right = List(StringType, StringType, ListType))

  override def perform(args: Array[Argument], context: Context): Unit = {
    val path = context.asInstanceOf[ExtensionContext].workspace.fileManager.attachPrefix(args(0).getString)

    try {

      val fileName: String = args(0).getString
      val fileContent: Seq[LogoList] = args(2).getList.map(l => l.asInstanceOf[LogoList])
      val serializationType: String = args(1).getString

      XesWriter.write(fileName, fileContent, serializationType)

    } catch {
      case e: FileNotFoundException => throw new ExtensionException("Couldn't create file: " + path, e)
      case e: IOException => throw new ExtensionException("Couldn't write to file: " + path, e)
      case e: Exception => throw new ExtensionException(s"Exception: ${e.getMessage}")
    }
  }
}

case class FileParserPrimitive(process: Seq[XLog] => LogoList) extends Reporter {
  override def getSyntax: Syntax = reporterSyntax(right = List(StringType | RepeatableType), ret = ListType, defaultOption = Some(1))

  override def report(args: Array[Argument], context: Context): LogoList = {
    val path = context.asInstanceOf[ExtensionContext].workspace.fileManager.attachPrefix(args(0).getString)
    val eventLogs = getLogsFromFile(path)

    process(eventLogs)

  }
}
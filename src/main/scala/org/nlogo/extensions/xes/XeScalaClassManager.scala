package org.nlogo.extensions.xes

import org.nlogo.api._

class XeScalaClassManager extends DefaultClassManager {

  import org.nlogo.extensions.xes.core.XesReader._

  def load(manager: PrimitiveManager) {
    val add = manager.addPrimitive _
    add("to-file", ToFile(Dump.logoObject))
    add("from-file", FileParserPrimitive(parseValue))
  }
}
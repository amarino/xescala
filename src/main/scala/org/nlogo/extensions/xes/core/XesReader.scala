package org.nlogo.extensions.xes.core

import java.io.File
import java.util

import org.apache.log4j.Logger
import org.deckfour.xes.classification.XEventClassifier
import org.deckfour.xes.extension.XExtension
import org.deckfour.xes.in.{XParser, XParserRegistry}
import org.deckfour.xes.model.{XAttribute, XElement, XLog}
import org.nlogo.api.ExtensionException
import org.nlogo.core.{LogoList, NumberParser}

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
  * @author Eugenio Liso and Alessandro Marino
  */
object XesReader {

  private val logger = Logger.getLogger("XesReader")

  def getLogsFromFile(path: String): Seq[XLog] = {
    val file: File = new File(path)

    val parsers = XParserRegistry.instance().getAvailable.asScala.toList

    logger.info("Loaded available parsers:" + parsers.mkString(","))

    val logs: Seq[XLog] = parseFile(parsers, file).asInstanceOf[java.util.List[XLog]].asScala

    logs
  }


  //Can be better
  def parseFile(parsers: List[XParser], file: File): AnyRef = {
    parsers match {
      case currentParser :: otherParsers =>
        Try {
          logger.info("Trying parser: " + currentParser)
          currentParser.parse(file)
        } match {
          case Success(logs) =>
            logger.info("Succesfully parsed logs with parser: " + currentParser)
            logs
          case Failure(_) => parseFile(otherParsers, file)
        }

      case Nil =>
        val exceptionMessage = "The file has an unsupported format or there are no available parsers. Also check if the file " +
          "has extension .xes"
        logger.error(exceptionMessage)
        throw new ExtensionException(exceptionMessage)
    }
  }

  def parseValue: Seq[XLog] => LogoList = events => {

    val eventsForNlogo = events.map(log => {
      val attrLog = extractAttributesAsLogolist(log)
      val classifiers = extractClassifiers(log.getClassifiers)
      val globalEventsAttr = convertAttributes(log.getGlobalEventAttributes)
      val traceEventsAttr = convertAttributes(log.getGlobalTraceAttributes)
      val extensions = extractExtensions(log.getExtensions)

      val tracesForNlogo = LogoList.fromIterator(log.iterator().asScala.map(trace => {
        val traceAttr = extractAttributesAsLogolist(trace)

        val eventsForNlogo = LogoList.fromIterator(
          trace.iterator().asScala.map(event => extractAttributesAsLogolist(event))
        )

        LogoList.fromIterator(Iterator(traceAttr, eventsForNlogo))
      }))

      LogoList.fromIterator(Iterator(extensions, globalEventsAttr, traceEventsAttr, classifiers, attrLog, tracesForNlogo))

    }).toIterator

    LogoList.fromIterator(eventsForNlogo)
  }

  //Those logolists fromIterator should be refactored (with an implicit if possible)
  private def extractClassifiers(classifiers: util.List[XEventClassifier]): LogoList = {
    LogoList.fromIterator(
      classifiers.asScala.map(classifier => LogoList.fromIterator(Iterator(
        classifier.name(), LogoList.fromIterator(classifier.getDefiningAttributeKeys.iterator)
      ))).toIterator
    )
  }

  private def extractExtensions(extensions: util.Set[XExtension]): LogoList = {
    LogoList.fromIterator(
      extensions.asScala.map(ext => LogoList.fromIterator(Iterator(
        ext.getName, ext.getPrefix, ext.getUri.toString
      ))).toIterator
    )
  }

  private def convertAttributes(attributes: java.util.List[XAttribute]): LogoList = {
    LogoList.fromIterator(
      attributes.asScala.map(
        elem => LogoList.fromIterator(Iterator(elem.getKey, elem.toString)) //toString gives the value
      ).toIterator
    )
  }

  private def extractAttributesAsLogolist[E <: XElement](event: E): LogoList = {

    val eventsLogoList = for (keyAndValue <- event.getAttributes.entrySet().asScala) yield
      LogoList.fromIterator(
        Iterator(keyAndValue.getKey, transformValueToNlogoType(keyAndValue.getValue.toString))
      )

    LogoList.fromIterator(eventsLogoList.toIterator)
  }

  private def transformValueToNlogoType(value: String): AnyRef =
    NumberParser.parse(value).right getOrElse (value.toUpperCase match {
      case "TRUE" => true: java.lang.Boolean
      case "FALSE" => false: java.lang.Boolean
      case _ => value
    })

}

package org.nlogo.extensions.xes.core

import java.io._

import org.apache.log4j.Logger
import org.deckfour.xes.factory.{XFactory, XFactoryRegistry}
import org.deckfour.xes.model.impl.XAttributeMapImpl
import org.deckfour.xes.model.{XAttributable, XEvent, XLog, XTrace}
import org.deckfour.xes.out.{XMxmlGZIPSerializer, XMxmlSerializer, XesXmlGZIPSerializer, XesXmlSerializer}
import org.nlogo.api.ExtensionException
import org.nlogo.core.LogoList

import scala.collection.JavaConverters._

/**
  * @author Eugenio Liso and Alessandro Marino
  */
object XesWriter {

  private val logger = Logger.getLogger("XesWriter")

  private var factory: XFactory = XFactoryRegistry.instance.currentDefault

  def walkLog(log: XLog): Unit = {
    walkAttributes(log)
    for (trace <- log.iterator.asScala.toSeq)
      walkTrace(trace)
  }

  def walkTrace(trace: XTrace): Unit = {
    walkAttributes(trace)
    for (event <- trace.iterator.asScala.toSeq)
      walkAttributes(event)
  }

  def walkAttributes(attributable: XAttributable): Unit = {
    val attributeMap = attributable.getAttributes
    for (attribute <- attributeMap.values.asScala.toSeq) {
      val key: String = attribute.getKey
      val value: String = attribute.toString
      println(key.trim + " " + value.trim)
      walkAttributes(attribute)
    }
  }

  def write(fileName: String, fileContent: Seq[LogoList], serializationType: String): Unit = {

    val xesLog: XLog = createLog(fileContent)

    try {
      val os: OutputStream = new BufferedOutputStream(new FileOutputStream(new File(fileName)))

      logger.info(s"Choosing $serializationType as serialization type. If not specified correctly " +
        s"(must be one of XML-GZIP, XML, XM-XML, XM-XML-GZIP), XML will be the default.")

      serializationType.toUpperCase match {
        case "XML-GZIP" => new XesXmlGZIPSerializer().serialize(xesLog, os)
        case "XM-XML" => new XMxmlSerializer().serialize(xesLog, os)
        case "XM-XML-GZIP" => new XMxmlGZIPSerializer().serialize(xesLog, os)
        case "XML" => new XesXmlSerializer().serialize(xesLog, os)
        case _ =>
          logger.warn(s"The specified serialization type: $serializationType is not among the supported types. Choosing XML as default.")
          new XesXmlSerializer().serialize(xesLog, os)
      }

      os.flush()
      os.close()
    } catch {
      case e: IOException =>
        val exceptionMessage = e.getMessage
        logger.error(exceptionMessage)
        throw new ExtensionException(exceptionMessage)
    }
  }

  def createLog(content: Seq[LogoList]): XLog = {

    import org.deckfour.xes.model.impl.XAttributeMapImpl

    val eventsToWrite: Seq[XEvent] = content.flatMap(logoList => {

      logger.info(s"Logolist Input: ${logoList.toString()}")

      val xAttributeMapImpl = new XAttributeMapImpl
      val xAttributes = logoList.map {

        //Not all types are enumerated
        case entry@(_: Double.type) => factory.createAttributeContinuous(entry.toString, entry.asInstanceOf[Double], null)
        case entry@(_: Boolean.type) => factory.createAttributeBoolean(entry.toString, entry.asInstanceOf[Boolean], null)
        case entry@(_: Long.type) => factory.createAttributeDiscrete(entry.toString(), entry.asInstanceOf[Long], null)
        case entry@(_: String) => factory.createAttributeLiteral(entry.toString, entry.asInstanceOf[String], null)
        case entry => factory.createAttributeLiteral(entry.toString, entry.toString, null)
      }

      xAttributes.foreach(elem => {
        xAttributeMapImpl.put(elem.getKey, elem)
      })

      logger.info(s"Attributes: ${xAttributes.toString()}")
      logger.info(s"Attributes map: ${xAttributeMapImpl.toString}")

      val events: Seq[XEvent] = createEvents(xAttributeMapImpl)

      logger.info(s"Events: ${events.toString()}")

      events
    })

    val traces = eventsToWrite.map(event => {
      val trace = factory.createTrace()
      trace.add(event)

      trace
    }) //Adding all the events. One trace per event

    logger.debug(s"Trace: ${traces.toString()}")

    val log: XLog = factory.createLog()
    traces.foreach(log.add) //Adding the traces

    logger.debug(s"Log: ${log.asScala.toString}")

    log
  }

  def createEvents(xAttributeMapImpl: XAttributeMapImpl): Seq[XEvent] =
    xAttributeMapImpl.asScala.map {
      case (key, value) =>
        val event = factory.createEvent()
        event.getAttributes.put(key, value)
        event
    }.toSeq

}

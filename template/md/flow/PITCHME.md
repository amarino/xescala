---
@title[Demo Content Title]

## NetLogo Demo Workflow

@snap[south]
@fa[arrow-down text-white]
@snapend

+++
@title[Demo content list]

@snap[north text-10]
XeScala Sample model on NetLogo
@snapend

@title[NetLogo Workflow]

@snap[midpoint list-content-concise span-100]
@ol
- Load NetLogo sample model
- Generate Objects
- Write Objects
- Read Objects
@olend
<br><br>
@snapend

+++
@title[Demo]

## Demo

@snap[south]
@fa[spinner fa-spin fa-2x]
@snapend

---
@title[Conclusion]

@snap[midpoint]
## Thanks for your attention!
@snapend

@snap[south]
@fa[laptop-code fa-2x]
@snapend
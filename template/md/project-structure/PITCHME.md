---
@title[Project Structure]

## XeScala Components

@snap[south]
@fa[arrow-down text-white]
@snapend

+++
@title[Classes]

@snap[north text-10]
XeS Classes
@snapend

@snap[center list-content-concise span-100]
@ol
- XeScalaClassManager
- XeScalaExtension
- XesReader
- XesWriter
@olend
@snapend

+++
@title[XES Class Manager]

## XES Class Manager

+++?code=src/main/scala/org/nlogo/extensions/xes/XeScalaClassManager.scala&lang=scala&title=XES Class Manager: Scala Snippet

@[5,7-13](XeScalaClassManager describes the primitives for NetLogo)

@snap[north-east template-note text-gray]
We use [OpenXES](http://www.xes-standard.org/openxes/start)
@snapend

+++
@title[XES Extension]

## XES Extension

+++?code=src/main/scala/org/nlogo/extensions/xes/XeSExtension.scala&lang=scala&title=Extension: Scala Snippet

@[14](ToFile Case Class represents a primitive)

@[18](getSyntaxt extracts input commands through the commandSyntax method)

@[20-27,29](The perform method invokes the write operation implemented into the XesWriter class)

@[39,44,46,49](FileParserPrimitive extracts logs through the process function `parseValue`)

@snap[north-east template-note text-gray]
We use [OpenXES](http://www.xes-standard.org/openxes/start)
@snapend

+++
@title[XES Reader]

## XES Reader

+++?code=src/main/scala/org/nlogo/extensions/xes/core/XesReader.scala&lang=scala&title=Reader: Scala Snippet

@[24-31](getLogsFromFile extracts a sequence of XLogs from the input file)

@[38,44-49](parseFile is a recursive method that consumes the list of available XParsers)

@[59-84](ParseValue function that constructs the result for NetLogo)

@snap[north-east template-note text-gray]
We use [OpenXES](http://www.xes-standard.org/openxes/start)
@snapend

+++
@title[XES Writer]

## XES Writer

+++?code=src/main/scala/org/nlogo/extensions/xes/core/XesWriter.scala&lang=scala&title=Writer: Scala Snippet

@[46](the `write` method applies a specific serialization type to the input file)
@[56-63](Available file serialization types)
@[88-92](Available data types that can be written to a XES file)